package net.sssubtlety.sss_version_catalog

import org.gradle.testkit.runner.GradleRunner
import spock.lang.Specification
import spock.lang.TempDir
import java.nio.file.Files

class VersionCatalog extends Specification {
    public static final String catalogPathString = "gradle/sssLibs.versions.toml"
    @TempDir File testProjectDir
    File catalogFile
    File settingsFile

    def setup() {
        new File(testProjectDir, 'gradle.properties').createNewFile()
        new File(testProjectDir, 'build.gradle').createNewFile()

        catalogFile = new File(testProjectDir, catalogPathString)
        catalogFile.getParentFile().mkdirs()
        Files.copy(new File(catalogPathString).toPath(), catalogFile.toPath())

        settingsFile = new File(testProjectDir, 'settings.gradle')
        settingsFile <<
            """
            dependencyResolutionManagement {
                repositories {
                    maven {
                        name = "sss_version_catalog"
                        url = "https://gitlab.com/api/v4/projects/55302875/packages/maven"
                    }
                }
                versionCatalogs {
                    sssLibs {
                        from(files('$catalogPathString'))
                    }
                }
            }
            """
    }

    def "build"() {
        when:
        GradleRunner.create().withProjectDir(testProjectDir).build()

        then:
        noExceptionThrown()
    }
}
